const express = require('express');
const cors = require('cors');
const http = require('http');


const app = express();
const server = http.createServer(app);
const { Server } = require('socket.io');
const io = new Server(server);

const users = {};
app.use(cors());
app.use(express.static("app"));

app.get('/', (req,res) => {
    res.sendFile(__dirname + '/app/index.html');
});

app.get('/users', (req,res) => {
    res.send(Object.values(users))
})

io.on('connection', socket => {
    console.log('connected', socket.id);
    socket.on('user-connected', user => {
        users[socket.id] = {
            ...user,
            id: socket.id,
            status: true
        };
        console.log("user-connected", users);
        console.log(users[socket.id]);
        socket.broadcast.emit('users-changed', Object.values(users));
    });

    socket.on('new-chat-message', message => {
        socket.to(message.recipientId).emit('new-chat-message', {
            text: message.text,
            senderId: socket.id,
        });
    });

    socket.on('disconnect', () => {
        users[socket.id];
        users[socket.id].status = false;
        socket.broadcast.emit('users-changed', Object.values(users));
    });
});

server.listen(3000, () => {
    console.log("listening on 3000 port")
});