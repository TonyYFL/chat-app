class WelcomeScreen{
    constructor() {
        this.$welcomeScreen = document.querySelector('.welcome-screen');
        this.$loginBtn = this.$welcomeScreen.querySelector('button');
        this.$input = this.$welcomeScreen.querySelector('input');

        this.initializeListeners();

    }

    hasNetwork(online) {
        console.log(online);
    }

    initializeListeners() {
        this.$loginBtn.addEventListener('click', () => {
            if(this.$input.value === ''){
                return;
            }

            const currentUser = {
                name: this.$input.value,
            }

            socket.emit('user-connected', currentUser);

            window.addEventListener("load", () => {
                hasNetwork()
            })

            this.$welcomeScreen.classList.add('hidden');
            new Chat({currentUser})
        })
    }
}